 <!-- footer content -->
        <footer>
          <div class="pull-right">
            Copyright &copy; <?php echo date('Y');?> 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
		
	
	<!-- jQuery -->
    
   
	<!-- Bootstrap -->
    
	{!!Html::script('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js')!!}
    <!-- FastClick -->
	{!!Html::script('assets/admin/vendors/fastclick/lib/fastclick.js')!!}
    
    <!-- NProgress -->
    
	{!!Html::script('assets/admin/vendors/nprogress/nprogress.js')!!}
    <!-- Chart.js -->
   
	{!!Html::script('assets/admin/vendors/Chart.js/dist/Chart.min.js')!!}
    <!-- gauge.js -->
   
	{!!Html::script('assets/admin/vendors/gauge.js/dist/gauge.min.js')!!}
    <!-- bootstrap-progressbar -->
   
	{!!Html::script('assets/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')!!}
    <!-- iCheck -->
    
	{!!Html::script('assets/admin/vendors/iCheck/icheck.min.js')!!}
    <!-- Skycons -->
    
	{!!Html::script('assets/admin/vendors/skycons/skycons.js')!!}
    <!-- Flot -->
    
	{!!Html::script('assets/admin/vendors/Flot/jquery.flot.js')!!}
	{!!Html::script('assets/admin/vendors/Flot/jquery.flot.pie.js')!!}
    {!!Html::script('assets/admin/vendors/Flot/jquery.flot.time.js')!!}
    {!!Html::script('assets/admin/vendors/Flot/jquery.flot.stack.js')!!}
    {!!Html::script('assets/admin/vendors/Flot/jquery.flot.resize.js')!!}
    
    <!-- Flot plugins -->
   
	{!!Html::script('assets/admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js')!!}
	{!!Html::script('assets/admin/vendors/flot-spline/js/jquery.flot.spline.min.js')!!}
	{!!Html::script('assets/admin/vendors/flot.curvedlines/curvedLines.js')!!}
    
    <!-- DateJS -->
    
	{!!Html::script('assets/admin/vendors/DateJS/build/date.js')!!}
    <!-- JQVMap -->
    
	{!!Html::script('assets/admin/vendors/jqvmap/dist/jquery.vmap.js')!!}
	{!!Html::script('assets/admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js')!!}
	{!!Html::script('assets/admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')!!}
   
    <!-- bootstrap-daterangepicker -->
	{!!Html::script('assets/admin/vendors/moment/min/moment.min.js')!!}
    
	{!!Html::script('assets/admin/vendors/bootstrap-daterangepicker/daterangepicker.js')!!}
    

    <!-- Custom Theme Scripts -->
   	
	{!!Html::script('assets/admin/build/js/custom.min.js')!!}
	
	
	{!!Html::script('assets/admin/js/jquery.dataTables.min.js')!!}
	{!!Html::script('assets/admin/js/dataTables.bootstrap.min.js')!!}
	{!!Html::script('assets/admin/js/validator.js')!!}
	
	{!!Html::script('assets/admin/js/canvasjs.min.js')!!}
	
    {!!Html::script('assets/admin/js/custom.js')!!}
    
    {!!Html::script('assets/js/tagsinput.js')!!}
    
     {!!Html::script('assets/admin/fontscript/jquery.fontselect.js')!!}
     {!!Html::script('assets/admin/fontscript/color.js')!!} 
     
     
     
     
     
     
     